provider "proxmox" {
  pm_api_url = "https://${var.pm_host["pm_api_url"]}/api2/json"
  pm_user = var.pm_host["pm_user"]
  pm_password = var.pm_host["pm_password"]
  pm_tls_insecure = true
}

resource "proxmox_vm_qemu" "rocky-1" {
  name = "rocky-1"
  iso = var.rocky
  target_node = var.node
}