variable "rocky" {
  type = string
  default = "local:iso/Rocky-8.4-x86_64-minimal.iso"
}

variable "node" {
  type = string
  default = "pve"
}